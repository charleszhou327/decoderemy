//============================================================================
// Name        : extractRemy.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <typeinfo>
#include "remy/whiskertree.hh"
#include "remy/dna.pb.h"
using namespace std;


int main(int argc, char* argv[]) {
	if (argc != 2) {
    cerr << "Usage:  " << argv[0] << " Input binary" << endl;
    return -1;
  }

	RemyBuffers::WhiskerTree tree;
	const char *binary_rule = argv[1];
	int fd = open(binary_rule, O_RDONLY);
	if (fd < 0) {
		perror("open");
		throw 1;
	}
	if (!tree.ParseFromFileDescriptor(fd)) {
		fprintf( stderr, "RemyTCP: Could not parse whiskers in \"%s\".\n",
				binary_rule);
		throw 1;
	}

	// close file
	if (::close(fd) < 0) {
		perror("close");
		throw 1;
	}

	// store whiskers to _whiskers and also local file
	const WhiskerTree *_whiskers = new WhiskerTree(tree);
	// if (_whiskers->write2file(argv[2]) < 0){
	// 	perror("Error when writing rules to file.");
	// 	throw 1;
	// }
}
