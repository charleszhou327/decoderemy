ruleID = fopen('rules');
C = textscan(ruleID,'%s %f %s %f %s %f %s %f %s %f %s %f %s %f %s %f %s %f');
fclose(ruleID);

N = length(C{1});
fields = 9;
matrix = zeros(N, 9);
for i = 1:fields
    matrix(:,i) = C{2*i};
end

cdfplot(matrix(:,9))

% hist(matrix(:,8), 50)
%scatter(matrix(:,1),  matrix(:,3))

%hold on
%[row,col] = find((matrix == 16384) | (matrix == 0));
%matrix(row, :) = [];
%plot([matrix(:,1),  matrix(:,4)], [matrix(:,3), matrix(:,6)])